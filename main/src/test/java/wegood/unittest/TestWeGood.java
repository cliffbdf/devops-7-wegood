package wegood.unittest;

import wegood.WeGood;
import java.time.LocalTime;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestWeGood {

	@Test
	public void testAreWeGood() {
		WeGood w = new WeGood();
		LocalTime time = LocalTime.of(18, 1);
		w.setTime(time);
		boolean result = w.areWeGood();
		assertEquals(true, result);
	}
}

package wegood;

import static spark.Spark.*;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.format.DateTimeParseException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WeGood {

	private LocalTime time = null;  // used for overriding current time, for testing.
	private Logger logger = LoggerFactory.getLogger(WeGood.class);

	public static void main(String[] args) {
		WeGood weGood = new WeGood();
		Logger logger = weGood.logger;

		//System.out.println(helloWorld.sayHi());
		get("/", (req, res) -> "WeGood Service");
		get("/arewegood", (req, res) -> {
			logger.debug("Received arewegood request");
			try {
				boolean areWeGood = weGood.areWeGood();
				AreWeGoodResponse response = new AreWeGoodResponse(areWeGood);
				Gson gson = new Gson();
				String json = json = gson.toJson(response);
				return json.toString();
			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});

		/* For overriding the current time. This endpoint is only used for testing. */
		get("/settime", (req, res) -> {

			try {
				LocalTime time = null;

				String timestr = req.queryParams("time");
				if ((timestr != null) && (! timestr.equals(""))) {
					try {
						time = LocalTime.parse(timestr);
					} catch (DateTimeParseException ex) {
						throw new Exception("time parameter:", ex);
					}
				}

				weGood.setTime(time);

				SetTimeResponse response = new SetTimeResponse();
				Gson gson = new Gson();
				String json = json = gson.toJson(response);
				return json.toString();
			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});
	}

	public boolean areWeGood() {
		LocalTime time = this.time;
		if (time == null) {
			time = LocalTime.now();
		}
		int hourOfDay = time.get(ChronoField.HOUR_OF_DAY);
		return ((hourOfDay >= 18) || (hourOfDay < 9));
	}

	public void setTime(LocalTime newTime) {
		this.time = newTime;
	}

	static class AreWeGoodResponse {
		public AreWeGoodResponse(boolean good) {
			this.areWeGood = good;
		}
		public boolean areWeGood;
	}

	static class SetTimeResponse {
	}
}

# Optional parameters: DOCKERHUB_USER_ID DOCKERHUB_PASSWORD
# Make sure to source the appropriate env file.

mvn clean install
#mvn clean install -DskipTests
if [ $? -ne 0 ]; then
	exit 1
fi

if [ -n "$SONAR_TOKEN" ]; then
	mvn sonar:sonar -pl !features -Dsonar.projectKey=DevOpsForAgileCoaches \
		-Dsonar.host.url=$SONAR_URL -Dsonar.login=$SONAR_TOKEN
fi
if [ $? -ne 0 ]; then
	exit 1
fi

# Build the container image:
if [ `uname` = 'Linux' ]; then
	mkdir -p scratch
	rm scratch/*
	cp main/target/*.jar scratch
	sudo docker build -t $IMAGE_NAME -f Dockerfile scratch
	if [ $? -ne 0 ]; then
		exit 1
	fi
	# Push image to Dockerhub:
	if [ "$#" -eq 2 ]; then
		sudo docker login -u $1 -p $2
		if [ $? -ne 0 ]; then
			exit 1
		fi
		sudo docker push $IMAGE_NAME
		if [ $? -ne 0 ]; then
			exit 1
		fi
	fi
fi

package wegood.stub;
public class WeGoodMockFactory implements WeGoodFactory {
    public WeGoodStub createWeGood() throws Exception {
        return new WeGoodMock();
    }
}

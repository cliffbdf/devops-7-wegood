package wegood.storytests;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.junit.Assert.*;

// Import the WeGood stub
import wegood.stub.WeGoodFactory;
import wegood.stub.WeGoodStub;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class BasicIT {

	private Boolean weGoodResponse = null;
	private WeGoodStub weGood = null;

	public BasicIT() throws Exception {

		try {
			// Create stub for accessing the WeGood service:
			String weGoodFactoryClassName = System.getenv("WEGOOD_FACTORY_CLASS_NAME");
			if (weGoodFactoryClassName == null) {
				throw new Exception("Env variable WEGOOD_FACTORY_CLASS_NAME is not defined");
			}
			WeGoodFactory weGoodFactory = (WeGoodFactory)
				(Class.forName(weGoodFactoryClassName).getDeclaredConstructor().newInstance());
			this.weGood = weGoodFactory.createWeGood();
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}
	}

	@Given("the time is {string}")
	public void set_time(String timestr) throws Exception {

		try {
			// Parse the time string:
			LocalTime localTime = LocalTime.parse(timestr, DateTimeFormatter.ISO_LOCAL_TIME); // Text '9:01' could not be parsed at index 0

			/* Tell the WeGood service to set the time to a specified value. Note that as
				soon as we set it, it starts increasing: */
			this.weGood.setTime(localTime);
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}
	}

	@When("I call WeGood")
	public void i_call_WeGood() throws Exception {
		try {
		    this.weGoodResponse = this.weGood.areWeGood();
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}
	}

	@Then("it returns true")
	public void it_returns_true() throws Exception {
		assertEquals(true, this.weGoodResponse);
	}

	@Then("it returns false")
	public void it_returns_false() throws Exception {
		assertEquals(false, this.weGoodResponse);
	}
}
